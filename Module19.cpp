#include <iostream>
using namespace std;

class Animal
{
public:

    Animal() {}

    virtual void Voice()
    {
        cout << "Animal's voice";
    }
};

class Dog : public Animal
{
public:

    Dog() {}

    void Voice() override
    {
        cout << "Woof! \n";
    }
};

class Cat : public Animal
{
public:

    Cat() {}

    void Voice() override
    {
        cout << "Meow! \n";
    }
};


class Cock : public Animal
{
public:

    Cock() {}

    void Voice() override
    {
        cout << "Cock - A - Doodle - Doo! \n";
    }
};




int main()
{
    Animal* v1 = new Dog();
    Animal* v2 = new Cat();
    Animal* v3 = new Cock();
    Animal* arr[] = {v1, v2, v3};
    
    for (int i = 0; i < size(arr); i++)
    {
        arr[i]->Voice;
    }
    
  
}
